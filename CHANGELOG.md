* v1.0
  * First stable release of LavCorps-Line Utility Dockers!
  * This release signifies stable test images as well as stable image
generation.
* v1.0.1
  * Fix for an issue where tagged image generation can't execute due to a bad
shebang
* v1.1
  * Add Scheduled Image Generation Support and More
* v1.1.1
  * Run image generation on `utility-dockers/img`
* v1.2
  * Cut down on redundancy in build pipeline + add alpine-sdk docker img
* v1.2.1
  * Add WIP Alpine-SDK, Allow jobs up to 3 attempts, LavCorps Git Base Updates
* v1.2.2
  * Migrate Utility Dockers to LavCorps-Line/Infrastructure
* v1.2.3
  * Workaround to provide stable img release
* v1.2.4
  * Finish Migrating Utility Dockers to LavCorps-Line/Infrastructure
* v1.2.5
  * Bring test builds up to bar with the alpine-dockers build process
* v1.2.6
  * Hotfix for scheduled build scripts with new git update
* v1.2.7
  * Update img dockerfile to use Alpine 3.16 instead of Alpine 3.15
* v2.0.0
  * Overhauls pipeline system to be dynamically generated from a dhall file,
deduplicating info and allowing for easier expansion.
* v2.1.0
  * Various fixes & tweaks
  * Add pydev, cabal, and cargo docker images
* v2.1.1
  * Don't add cabal & cargo on alpine-sdk
* v2.2.0
  * Add podman & buildkit dockerfiles
  * Update all dockerfiles to Alpine 3.17
  * Push Alpine-SDK, Cabal, Cargo to stable
* v2.2.1
  * replace MAINTAINER field with maintainer LABEL
* v2.2.2
  * add user to abuild in Alpine-SDK dockerfile
* v2.3.0
  * Update all utility dockers to Alpine 3.18
* v2.4.0
  * Various changes involving dockers and their contents.
* v3.0.0
  * BREAKING CHANGE: Img is no longer being built, as the project has been
inactive for around a year and hasn't pushed out an update in three. Please use
buildah on LavCorps-Line CI/CD systems instead.
* v3.0.1
  * Pivot Buildah container used from snapshot to stable